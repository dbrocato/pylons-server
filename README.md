### The Perkasie Setup

I'm documenting this now because I know I'll come to forget in the near future.

I have two routers at my home in Perkasie. One (a Verizon Fios Router) resides in the office and provides Wi-Fi/ethernet for everything on the second and third floors. The other (a TP-Link Archer A7) lives in the basement and provides an Wi-Fi/ethernet connection for the TVs in the basement and works as the "front" router, as it is directly connected to the Verizon cable modem. To connect the Verizon router to the internet, it is connected to the TP-Link via ethernet.

It took me several hours to troubleshoot and find that this setup was what was making it difficult to send data to my Raspberry Pi. Admittedly, I forgot that the basement router was acting as the primary router, so that accounts for the time it took.

Now, with this set-up, there's two ways to get data over the internet to the Raspberry Pi, and it depends on which router it's connected to.

#### Connected to the TP-Link

If the Raspberry Pi is connected to the TP-Link, then it's as simple as performing port-forwarding on the TP-Link router with the Raspberry Pi as the recipient. Additionally, to avoid any dynamic internal IP mishaps, the Raspberry Pi needs a reserved internal IP on the TP-Link.

#### Connected to the Verizon Router

If the Raspberry Pi is connected to the Verizon router, it's a bit more complex. First, the Raspberry PI needs a reserved internal IP on the Verizon router, and the Verizon router needs a reserved internal IP on the TP-Link.

Then, port-forwarding of the desired port needs to be performed on the TP-Link with the Verizon router as the target. Finally, port-forwarding must be performed on the Verizon router with the Raspberry Pi as the target.