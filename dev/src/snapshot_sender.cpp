#include "snapshot_sender.h"

#include <time.h>
#include <unistd.h>

SnapshotSender::SnapshotSender(Server* server, UnityGameInstance* game, int ticks_per_second)
{
    this->server = server;
    this->game = game;
    this->ticks_per_second = ticks_per_second;
}

SnapshotSender::~SnapshotSender()
{

}

void SnapshotSender::Launch()
{
    snapshot_sender_thread = std::thread(&SnapshotSender::SnapshotSenderThread, this);
}

void SnapshotSender::ShutDown()
{
    shutdown_lock.lock();
    shutdown = true;
    shutdown_lock.unlock();

    snapshot_sender_thread.join();
}

void SnapshotSender::SnapshotSenderThread()
{
    for (;;)
    {
        shutdown_lock.lock();
        if (shutdown)
        {
            shutdown_lock.unlock();
            return;
        }
        shutdown_lock.unlock();

        WritablePacket* snapshot = game->BuildSnapshot();
        server->SendPacketToAllClients(snapshot);
        delete snapshot;

        usleep(30000);
    }
}