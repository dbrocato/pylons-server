#include "unity_game_instance.h"

#include <iostream>

UnityGameInstance::UnityGameInstance()
{
    trackers = std::unordered_map<int, Tracker*>();
    trackers.clear();
}

UnityGameInstance::~UnityGameInstance()
{
    // Delete all trackers in map. TODO: Ensure they aren't deleted elsewhere.
    // TODO: Ensure thread safety?
    for (std::unordered_map<int, Tracker*>::iterator iter = trackers.begin(); iter != trackers.end(); iter++)
    {
        delete iter->second;
    }
}

void UnityGameInstance::Track(ReadablePacket* packet)
{
    // TODO: Optimize this mess.

    int tracker_id;
    packet->Read(tracker_id);

    try
    {
        Tracker* existing_tracker = trackers.at(tracker_id);

        // Tracker already exists server-side, so just update it.

        existing_tracker->data_lock.lock();

        int data_size;
        packet->Read(data_size);
        unsigned char data[data_size];
        packet->Read(data, data_size);
        existing_tracker->SetData(std::vector<unsigned char>(data, data + data_size));

        // Mark it for a snapshot update.
        existing_tracker->has_unsent_update = true;

        existing_tracker->data_lock.unlock();
    }
    catch (const std::exception& e)
    {
        // Tracker does not exist server-side, so add it.
        // TODO: Ensure thread safety.

        std::string description;
        packet->Read(description);

        int data_size;
        packet->Read(data_size);
        unsigned char data[data_size];
        packet->Read(data, data_size);

        trackers.emplace(tracker_id, new Tracker(tracker_id, description, std::vector<unsigned char>(data, data + data_size)));
    }
}

void UnityGameInstance::Untrack(int tracker_id)
{
    // TODO: Ensure thread safety.
    try
    {
        Tracker* tracker = trackers.at(tracker_id);
        trackers.erase(tracker_id);
        delete tracker;
    }
    catch (const std::exception& e)
    {
        // Entry not found.
        return;
    }
}

WritablePacket* UnityGameInstance::BuildSnapshot()
{
    WritablePacket* snapshot = new WritablePacket(1024);

    for (std::unordered_map<int, Tracker*>::iterator iter = trackers.begin(); iter != trackers.end(); iter++)
    {
        iter->second->data_lock.lock();
        if (iter->second->has_unsent_update)
        {
            snapshot->Write(static_cast<std::underlying_type<UnityCommand>::type>(UnityCommand::S_UpdateSubscribeTracker));
            snapshot->Write(iter->first); // Write tracker ID.

            std::vector<unsigned char> data = iter->second->GetData();
            for (size_t i = 0; i < data.size(); i++)
            {
                snapshot->Write(data[i]);
            }

            iter->second->has_unsent_update = false;
        }
        iter->second->data_lock.unlock();
    }

    return snapshot;
}