#include "client.h"

#include <cstring>

Client::Client()
{
    this->client_id = -1;
}

Client::Client(int client_id, sockaddr_in client_addr, socklen_t client_addrlen)
{
    this->client_id = client_id;
    this->client_addr = client_addr;
    this->client_addrlen = client_addrlen;
}

int Client::GetClientID()
{
    return client_id;
}

sockaddr_in Client::GetSockaddr()
{
    return client_addr;
}

socklen_t Client::GetAddrlen()
{
    return client_addrlen;
}