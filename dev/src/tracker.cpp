#include "tracker.h"

Tracker::Tracker(int tracker_id, std::string description)
{
    this->tracker_id = tracker_id;
    this->description = description;
    this->has_unsent_update = false;
}


Tracker::Tracker(int tracker_id, std::string description, std::vector<unsigned char> data)
{
    this->tracker_id = tracker_id;
    this->description = description;
    this->data = data;
    this->has_unsent_update = true;
}

Tracker::~Tracker()
{
    
}

int Tracker::GetTrackerID()
{
    return tracker_id;
}

std::string Tracker::GetDescription()
{
    return description;
}

std::vector<unsigned char> Tracker::GetData()
{
    return data;
}

void Tracker::SetData(std::vector<unsigned char> data)
{
    this->data = data;
}