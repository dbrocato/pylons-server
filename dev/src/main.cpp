#include "globals.h"

#include "server.h"
#include "unity_game_instance.h"
#include "packet_handler.h"
#include "snapshot_sender.h"

#include <iostream>
#include <cstring>

#define DEFAULT_PORT 18944
#define DEFAULT_NUM_THREADS 1
#define DEFAULT_VERBOSE false

int port = DEFAULT_PORT;
int num_server_threads = DEFAULT_NUM_THREADS;
bool verbose = DEFAULT_VERBOSE;

static void print_usage()
{
    std::printf("\t-h\t\tDisplay this usage guide\n");
    std::printf("\t-v\t\tEnable verbose. (Default: Disabled.)\n");
    std::printf("\t-p port\t\tSpecify port number. (Default: 18944)\n");
    std::printf("\t-t num_threads\t\tSpecify number of server threads. (Default: 1)\n");
}

static void handle_args(int argc, const char* argv[])
{
    // Search for -h first.
    for (int i = 1; i < argc; i++)
    {
        if (strcmp("-h", argv[i]) == 0)
        {
            print_usage();
            exit(0);
        }
    }

    // Handle other args.
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-v") == 0)
        {
            verbose = true;
        }

        else if (strcmp(argv[i], "-p") == 0)
        {
            port = atoi(argv[++i]);
        }

        else if (strcmp(argv[i], "-t") == 0)
        {
            num_server_threads = atoi(argv[++i]);
        }

        else // Handle case where argument doesn't exist.
        {
            print_usage();
            exit(0);
        }
    }
}

int main(int argc, const char* argv[])
{
    std::printf("\n--- Welcome to the Pylons Game Server! Author: Damien Brocato. ---\n");

    handle_args(argc, argv);

    // Print exit method.
    std::printf("\n*** Press any key at any time to shut down the server. ***\n\n");

    Server* server = new Server();
    UnityGameInstance* game = new UnityGameInstance();
    PacketHandler* packet_handler = new PacketHandler(server, game);
    SnapshotSender* snapshot_sender = new SnapshotSender(server, game, 1);

    if (server->Launch() == -1)
    {
        std::printf("There was a problem launching the server. Exiting.\n");
        return -1;
    }
    std::printf("Server successfully started on port %d\n", port);

    std::printf("Attempting to start %d PacketHandlerThread threads...\n", num_server_threads);
    packet_handler->Launch(num_server_threads);
    std::printf("%d PacketHandlerThread threads successfully started.\n", num_server_threads);

    std::printf("Attempting to start the SnapshotSender thread...\n");
    snapshot_sender->Launch();
    std::printf("SnapshotSender thread successfully started.\n");


    // Listen for exit indication.
    std::getchar();

    std::printf("Shutting down the SnapshotSender thread...\n");
    snapshot_sender->ShutDown();
    std::printf("SnapshotSender thread successfully shut down.\n");

    std::printf("Shutting down %d PacketHandlerThread threads...\n", num_server_threads);
    packet_handler->ShutDown();
    std::printf("%d PacketHandlerThread threads successfully shut down.\n", num_server_threads);

    std::printf("Server shutting down...\n");

    if (server->ShutDown() == -1)
    {
        std::printf("There was a problem shutting down the server. Exiting.\n");
        return -1;
    }

    std::printf("Server successfully shut down.\n");

    delete packet_handler;
    delete game;
    delete server;

    return 0;
}