#include "server.h"

#include "globals.h"

#include <iostream>
#include <exception>

#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

Server::Server()
{
    clients.clear();
}

Server::~Server()
{

}

int Server::Launch()
{
    // Open UDP socket.
    addrinfo* info;
    addrinfo* pinfo;
    addrinfo hint;
    memset(&hint, 0, sizeof(addrinfo));

    hint.ai_flags = AI_PASSIVE | AI_NUMERICSERV;
    hint.ai_protocol = IPPROTO_UDP;
    if (getaddrinfo(NULL, std::to_string(port).c_str(), &hint, &info) != 0)
    {
        perror("getaddrinfo");
        return -1;
    }

    // TODO: Support IPv6?

    // Find IPv4 address.
    for (pinfo = info; pinfo; pinfo = pinfo->ai_next)
    {
        if (pinfo->ai_family == AF_INET) break;
    }
    
    // Open socket.
    socket_fd = socket(pinfo->ai_family, pinfo->ai_socktype, pinfo->ai_protocol);
    if (socket_fd == -1)
    {
        perror("socket");
        return -1;
    }

    // Set recv timeout.
    setsockopt(socket_fd, SOL_SOCKET, SO_RCVTIMEO, &RECV_TIMEOUT, sizeof(timeval));

    // Bind address to socket.
    if (bind(socket_fd, pinfo->ai_addr, pinfo->ai_addrlen) == -1)
    {
        perror("bind");
        close(socket_fd);
        return -1;
    }

    return 0;
}

int Server::ShutDown()
{
    // Close socket.
    if (close(socket_fd) == -1)
    {
        perror("close");
        return -1;
    }

    return 0;
}

ReadablePacket* Server::ReceivePacketFromAnyClient(sockaddr* client_addr, socklen_t* client_addrlen)
{
    // TODO: Un-hardcode all 1024s.

    // Create local recv output buffer.
    unsigned char buffer[1024];
    memset(buffer, 0, 1024);

    // Make recvfrom call, which is thread safe. It will time out based on the RECV_TIMEOUT in server.h.
    int bytes_received = recvfrom(socket_fd, buffer, 1024, 0, client_addr, client_addrlen);

    if (bytes_received > 0)
    {
        //std::cout << "Bytes Received: " << bytes_received << std::endl;
        return new ReadablePacket(buffer, bytes_received);
    }
    else
    {
        return nullptr;
    }
}

int Server::SendPacketToClient(int client_id, WritablePacket* packet)
{
    try
    {
        clients.at(client_id);
    }
    catch(const std::exception& e)
    {
        std::cout << "Error in Server::SendPacketToClient: client_id " << client_id << " not found." << std::endl;
        return 0;
    }
    
    sockaddr_in dest = clients.at(client_id).GetSockaddr();

    int bytes_sent = sendto(socket_fd, packet->GetByteArray(), packet->GetSize(), 0, (const struct sockaddr*)&dest, clients.at(client_id).GetAddrlen());

    if (bytes_sent == -1)
    {
        perror("sendto");
        exit(-1);
    }

    return bytes_sent;
}

int Server::SendPacketToAllClients(WritablePacket* packet)
{
    for (std::unordered_map<int, Client>::iterator iter = clients.begin(); iter != clients.end(); iter++)
    {
        SendPacketToClient(iter->first, packet);
    }
    return 0;
}

void Server::AddClient(sockaddr_in client_addr, socklen_t client_addrlen)
{
    std::printf("Adding new client...\n");
    int client_id = clients.size() + 1; // Start client indexing at 1.
    clients.emplace(client_id, Client(client_id, client_addr, client_addrlen));

    // Construct and send reply which only contains the client's assigned ID.
    WritablePacket packet = WritablePacket(1024);
    packet.Write(client_id);
    SendPacketToClient(client_id, &packet);
}