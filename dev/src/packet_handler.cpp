#include "packet_handler.h"
#include "unity_data_types.h"

#include <sys/socket.h>
#include <netinet/in.h>

#include <iostream>
#include <cstring>


PacketHandler::PacketHandler(Server* server, UnityGameInstance* game)
{
    this->server = server;
    this->game = game;
}

PacketHandler::~PacketHandler()
{

}


void PacketHandler::Launch(int num_threads)
{
    this->num_threads = num_threads;
    packet_handler_threads = std::vector<std::thread>(num_threads);

    for (int i = 0; i < num_threads; i++)
    {
        packet_handler_threads[i] = std::thread(&PacketHandler::PacketHandlerThread, this, i);
    }
}

void PacketHandler::ShutDown()
{
    shutdown_lock.lock();
    shutdown = true;
    shutdown_lock.unlock();

    for (int i = 0; i < num_threads; i++)
    {
        packet_handler_threads[i].join();
    }
}


void PacketHandler::PacketHandlerThread(int thread_num)
{
    std::printf("[PACKET HANDLER THREAD %i] Beginning data reception loop.\n", thread_num);
    for (;;)
    {
        // Check for shutdown.
        shutdown_lock.lock();
        if (shutdown)
        {
            shutdown_lock.unlock();
            return;
        }
        shutdown_lock.unlock();

        // Initialize recvfrom return parameters.
        sockaddr_in client_addr;
        memset(&client_addr, 0, sizeof(client_addr));

        socklen_t client_addrlen = sizeof(sockaddr_in);

        ReadablePacket* packet = server->ReceivePacketFromAnyClient((struct sockaddr*)&client_addr, &client_addrlen);

        if (packet != nullptr)
        {
            // Read header.
            int client_id;
            packet->Read(client_id);

            if (client_id == 0) // Client ID of 0 indicates that this client is unregistered.
            {
                server->AddClient(client_addr, client_addrlen);
            }
            else
            {
                while (packet->HasNext())
                {
                    unsigned char command_byte;
                    packet->Read(command_byte);
                    UnityCommand command = (UnityCommand)command_byte;

                    try
                    {
                        command_handlers.at(command);
                    }
                    catch(const std::exception& e)
                    {
                        std::cout << "Error: Command " << (int)command_byte << " unsupported." << std::endl;
                        // TODO: Instead of exiting, request a re-send from the client to check for packet loss.
                        exit(-1);
                    }
                    
                    (this->*(command_handlers.at(command)))(client_id, packet);
                }
            }
        }

        delete packet;
    }
}


void PacketHandler::Ping(int client_id, ReadablePacket* packet)
{
    double ms;

    packet->Read(ms);

    WritablePacket ping_resp_packet = WritablePacket(1024);
    ping_resp_packet.Write(static_cast<std::underlying_type<UnityCommand>::type>(UnityCommand::C_Ping));
    ping_resp_packet.Write(ms);

    server->SendPacketToClient(client_id, &ping_resp_packet);
}

void PacketHandler::PrintChar(int client_id, ReadablePacket* packet)
{
    char data;
    packet->Read(data);
    std::cout << "Print char data: " << data << std::endl;
}

void PacketHandler::PrintShort(int client_id, ReadablePacket* packet)
{
    short data;
    packet->Read(data);
    std::cout << "Print short data: " << data << std::endl;
}

void PacketHandler::PrintInt(int client_id, ReadablePacket* packet)
{
    int data;
    packet->Read(data);
    std::cout << "Print int data: " << data << std::endl;
}

void PacketHandler::PrintLong(int client_id, ReadablePacket* packet)
{
    long data;
    packet->Read(data);
    std::cout << "Print long data: " << data << std::endl;
}

void PacketHandler::PrintFloat(int client_id, ReadablePacket* packet)
{
    float data;
    packet->Read(data);
    std::cout << "Print float data: " << data << std::endl;
}

void PacketHandler::PrintDouble(int client_id, ReadablePacket* packet)
{
    double data;
    packet->Read(data);
    std::cout << "Print double data: " << data << std::endl;
}

void PacketHandler::PrintBool(int client_id, ReadablePacket* packet)
{
    bool data;
    packet->Read(data);
    std::cout << std::boolalpha << "Print bool data: " << data << std::endl;
}

void PacketHandler::PrintString(int client_id, ReadablePacket* packet)
{
    std::string data;
    packet->Read(data);
    std::cout << "Print string data: " << data << std::endl;
}

void PacketHandler::TrackPublishTracker(int client_id, ReadablePacket* packet)
{
    game->Track(packet);
}

void PacketHandler::UpdatePublishTracker(int client_id, ReadablePacket* packet)
{
    game->Track(packet);
}

void PacketHandler::UntrackPublishTracker(int client_id, ReadablePacket* packet)
{
    int tracker_id;
    packet->Read(tracker_id);
    game->Untrack(tracker_id);
}