#ifndef PACKET_HANDLER_H
#define PACKET_HANDLER_H

#include "server.h"
#include "unity_game_instance.h"
#include "unity_command.h"
#include "packets.h"

#include <vector>
#include <unordered_map>
#include <thread>
#include <mutex>

/**
 * The PacketHandler object is top-level. It creates, manages, and eventually
 * joins threads that unpack and handle packets sent to the server from the client.
 * 
 * It is primarily characterized by its PacketHandlerThread function which
 * represents an "infinite" loop of receiving data from clients and updating
 * the sever-side game representation with that data. It can also respond to
 * the client when necessary, such as for a ping request that requires an
 * immediate response.
 * 
 * It uses the Server object passed in through the constructor to send and receive
 * packets and updates the UnityGameInstance object also passed in via the constructor
 * accordingly.
 */
class PacketHandler
{
    public:

        PacketHandler(Server* server, UnityGameInstance* game);
        ~PacketHandler();

        void Launch(int num_threads);
        void ShutDown();

    private: // Core fields and functions.

        Server* server;
        UnityGameInstance* game;

        int num_threads;
        std::vector<std::thread> packet_handler_threads;

        std::mutex shutdown_lock = std::mutex();
        bool shutdown = false;

        void PacketHandlerThread(int thread_num);

    private: // Handler functions, one for each individual command.

        void Ping(int client_id, ReadablePacket* packet);

        void PrintChar(int client_id, ReadablePacket* packet);
        void PrintShort(int client_id, ReadablePacket* packet);
        void PrintInt(int client_id, ReadablePacket* packet);
        void PrintLong(int client_id, ReadablePacket* packet);
        void PrintFloat(int client_id, ReadablePacket* packet);
        void PrintDouble(int client_id, ReadablePacket* packet);
        void PrintBool(int client_id, ReadablePacket* packet);
        void PrintString(int client_id, ReadablePacket* packet);

        void TrackPublishTracker(int client_id, ReadablePacket* packet);
        void UpdatePublishTracker(int client_id, ReadablePacket* packet);
        void UntrackPublishTracker(int client_id, ReadablePacket* packet);

    private: // Handler function map.

        typedef void (PacketHandler::*CommandHandler)(int, ReadablePacket*);

        std::unordered_map<UnityCommand, CommandHandler> command_handlers =
        {
            { UnityCommand::C_Ping,                     &PacketHandler::Ping },

            { UnityCommand::PrintChar,                  &PacketHandler::PrintChar },
            { UnityCommand::PrintShort,                 &PacketHandler::PrintShort },
            { UnityCommand::PrintInt,                   &PacketHandler::PrintInt },
            { UnityCommand::PrintLong,                  &PacketHandler::PrintLong },
            { UnityCommand::PrintFloat,                 &PacketHandler::PrintFloat },
            { UnityCommand::PrintDouble,                &PacketHandler::PrintDouble },
            { UnityCommand::PrintBool,                  &PacketHandler::PrintBool },
            { UnityCommand::PrintString,                &PacketHandler::PrintString },

            { UnityCommand::C_TrackPublishTracker,      &PacketHandler::TrackPublishTracker },
            { UnityCommand::C_UpdatePublishTracker,     &PacketHandler::UpdatePublishTracker },
            { UnityCommand::C_UntrackPublishTracker,    &PacketHandler::UntrackPublishTracker },
        };
};

#endif