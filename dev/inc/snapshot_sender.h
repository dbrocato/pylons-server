#ifndef SNAPSHOT_SENDER_H
#define SNAPSHOT_SENDER_H

#include "server.h"
#include "unity_game_instance.h"
#include "unity_command.h"
#include "packets.h"

#include <thread>
#include <mutex>

class SnapshotSender
{
    public:

        SnapshotSender(Server* server, UnityGameInstance* game, int ticks_per_second);
        ~SnapshotSender();

        void Launch();
        void ShutDown();

    private:

        Server* server;
        UnityGameInstance* game;

        int ticks_per_second;

        std::thread snapshot_sender_thread;

        std::mutex shutdown_lock = std::mutex();
        bool shutdown = false;

        void SnapshotSenderThread();
};

#endif