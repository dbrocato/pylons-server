#ifndef CLIENT_H
#define CLIENT_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

class Client
{
    public:

        Client();
        Client(int client_id, sockaddr_in client_addr, socklen_t addrlen);

        int GetClientID();
        sockaddr_in GetSockaddr();
        socklen_t GetAddrlen();


    private:
    
        int client_id;
        sockaddr_in client_addr;
        socklen_t client_addrlen;
};

#endif