#ifndef UNITY_COMMANDS_H
#define UNITY_COMMANDS_H

#include <cstdint>

/* Commands that the packet handler will use to determine the usage
of the packet. Each command is represented by an unsigned char (single
byte), so there can be up to 256 commands. Commands intended to be from
server to client are prefixed with S_ while commands intended to be from
client to server are prefixed with C_. */
enum class UnityCommand : unsigned char
{
    /* Setting each enum to its intended value for both code
    assurance and legibility. */

    /* Ping command sent by the client. Expects an immediate response
    from the server. */
    C_Ping                                  = 0,

    /* Print commands indicate that there is a single entry
    of that data type to be read and printed. They are mostly
    used for debugging. */

    PrintChar                               = 1, 
    PrintShort                              = 2,
    PrintInt                                = 3,
    PrintLong                               = 4,
    PrintFloat                              = 5,
    PrintDouble                             = 6,
    PrintBool                               = 7,
    PrintString                             = 8,

    C_TrackPublishTracker                   = 9,
    C_UpdatePublishTracker                  = 10,
    C_UntrackPublishTracker                 = 11,

    S_UpdateSubscribeTracker                = 12,
};

#endif