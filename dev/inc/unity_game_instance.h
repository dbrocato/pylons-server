#ifndef UNITY_GAME_INSTANCE_H
#define UNITY_GAME_INSTANCE_H

#include <unordered_map>

#include "unity_data_types.h"
#include "unity_command.h"
#include "tracker.h"
#include "packets.h"

// MASSIVE TODO: Move packet handlers to their own class. Make this a singleton?

class UnityGameInstance
{
    public:
        
        UnityGameInstance();
        ~UnityGameInstance();

        void Track(ReadablePacket* packet);
        void Untrack(int tracker_id);

        WritablePacket* BuildSnapshot();

    private:

        std::unordered_map<int, Tracker*> trackers;
};

#endif