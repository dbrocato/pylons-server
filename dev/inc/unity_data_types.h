#ifndef UNITY_FILE_TYPES_H
#define UNITY_FILE_TYPES_H

#include <ostream>

struct Vector2
{
    float x, y;

    Vector2()
    {
        this->x = 0;
        this->y = 0;
    }

    Vector2(float x, float y)
    {
        this->x = x;
        this->y = y;
    }

    friend std::ostream & operator << (std::ostream &out, const Vector2 &vec)
    {
        out << "(" << vec.x << ", " << vec.y << ")";
        return out;
    }
};

struct Vector3
{
    float x, y, z;

    Vector3()
    {
        this->x = 0;
        this->y = 0;
        this->z = 0;
    }

    Vector3(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    friend std::ostream & operator << (std::ostream &out, const Vector3 &vec)
    {
        out << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
        return out;
    }
};

struct Quaternion
{
    float w, x, y, z;

    Quaternion()
    {
        this->w = 0;
        this->x = 0;
        this->y = 0;
        this->z = 0;
    }

    Quaternion(float w, float x, float y, float z)
    {
        this->w = w;
        this->x = x;
        this->y = y;
        this->z = z;
    }

    friend std::ostream & operator << (std::ostream &out, const Quaternion &quaternion)
    {
        out << "(" << quaternion.w << ", " << quaternion.x << ", " << quaternion.y << ", " << quaternion.z << ")";
        return out;
    }
};

struct Transform
{
    struct Vector3 position;
    struct Quaternion rotation;
    struct Vector3 local_scale;

    Transform()
    {
        this->position = Vector3();
        this->rotation = Quaternion();
        this->local_scale = Vector3();
    }

    Transform(struct Vector3 position, struct Quaternion rotation, struct Vector3 local_scale)
    {
        this->position = position;
        this->rotation = rotation;
        this->local_scale = local_scale;
    }
};

#endif