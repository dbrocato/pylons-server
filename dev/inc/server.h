#ifndef SERVER_H
#define SERVER_H

#include "client.h"
#include "packets.h"

#include <thread>
#include <mutex>
#include <unordered_map>

#include <sys/time.h>

#define MAX_CLIENTS 10 // TODO: Make this a global?

#define RECV_TIMEOUT_SEC 1
#define RECV_TIMEOUT_USEC 0

/**
 * The Server object is top-level. It facilitates communication to and from
 * game clients by maintaining a list of active clients and the means to send
 * data to any or all of them or receive data from any of them. TODO: It also keeps
 * record of the most recently sent and received packets.
 * 
 * It is purely UDP (both read and write). Currently only supports IPv4. TODO: Support IPv6.
 * 
 * By itself, it does not affect client-side or server-side game state. It
 * simply fulfills the subset of responsibilites described above.
 */
class Server
{
    public:

        Server();
        ~Server();

        int Launch();
        int ShutDown();

        ReadablePacket* ReceivePacketFromAnyClient(sockaddr* client_addr, socklen_t* client_addrlen);
        int SendPacketToClient(int client_id, WritablePacket* packet);
        int SendPacketToAllClients(WritablePacket* packet);

        void AddClient(sockaddr_in client_addr, socklen_t client_addrlen);

    private:

        std::unordered_map<int, Client> clients = std::unordered_map<int, Client>(MAX_CLIENTS);

        /* Note that this timeout ultimately affects the shutdown time of the server, since
        each call to recv must time out before its thread can check the shutdown condition. */
        static constexpr timeval RECV_TIMEOUT = {RECV_TIMEOUT_SEC, RECV_TIMEOUT_USEC};

        int socket_fd;
};

#endif