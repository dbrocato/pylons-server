#ifndef TRACKER_H
#define TRACKER_H

#include <string>
#include <vector>
#include <mutex>
#include <thread>

class Tracker
{
    public:

        Tracker(int tracker_id, std::string description);
        Tracker(int tracker_id, std::string description, std::vector<unsigned char> data);
        ~Tracker();

    private:

        int tracker_id;
        std::string description;
        std::vector<unsigned char> data;

    public:

        std::mutex data_lock = std::mutex();
        bool has_unsent_update;

    public:

        int GetTrackerID();
        
        std::string GetDescription();
        
        std::vector<unsigned char> GetData();
        void SetData(std::vector<unsigned char> data);
};

#endif